Multi Website Cache Flush (MWCF)
--------------------------------

DESCRIPTION
--------------------------------
This module enables you to flush the entire Drupal cache on multiple websites. If you have the websites with the URLs

http://www.website-A.com
http://www.website-B.com
http://www.website-C.com
etc.

You can clear their cache at once. Just by calling an URL on the master site. Master site means the site,
where the URLs from the specific websites are contained in a config file. The cache flush events are logged by the
Drupal watchdog. The URL calling happens via PHP CURL.

USAGE
--------------------------------
After you've installed the module, the configuration template file

mwcf.template.conf.php

needs to be copied from the module's folder into the site configuration folder and renamed to

mwcf.conf.php

The site configuration folder is usually located in the following path:

/sites/default

Mind a different folder location on a Drupal multi-site setup.

The config file was choosen, because a configuration file can be easily deployed and is de-coupled from the database.

Specify there the URLs on which you want to clear the cache. Additionally you have to specify a cache clear key.

The "$aUrls"-array needs to be only filled on the master website from which you initialize the cache flush on the
multiple sites. The value from the "$sCacheClearKey"-variable needs to specified on all Drupal websites.

After successfully installing and configuring the module, you can call the master website via URL to flush the caches
on multiple websites. Like in the following example:

http://www.my-master-website.com/ccev

The path "ccev" runs the action and is a abbreviation for "clear cache everywhere".

EXTRA FUNCTIONS
--------------------------------

Fast cache clear for one Drupal site
------------

If you have the granted permission "Access URL fo flush entire cache on single site.", so you can clear the entir
single site cache via a simple URL call (f.e. in your browser or cURL). This URL is also called internally on each
site via a GET-parameter, if you use the "cache clear everywhere"-functionality (/ccev). The URL for this case is:

http://your-website-domain.com/ccall

Fast cache "rebuild" for one Drupal site
------------

Furthermore you can clear the entire single site cache and call the frontpage for a simple cache rebuild. That way an
new visitor on the site has not to wait for the cache rebuild, which takes a while. The URL for this case is:

http://your-website-domain.com/ccall_rebuild