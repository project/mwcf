<?php

/**
 * The URLs of the Drupal (7) websites, that should be flushed. Configuration
 * only necessary on the master Drupal website.
 */
$aUrls = array(
  'http://website-one.com',
  'http://website-two.com'
);

/**
 * The key which will allow the cache flush. Edit it to any custom unique
 * string.
 */
$sCacheClearKey = '124536jhasdkhsdksdf';

/**
 * This boolean variable can be set either to "1" (enabled) or "0" (disabled).
 * If this variable is set to "1", the domain of the flushed site will be
 * called and Drupal will re-build caches. That way an new visitor on the
 * page will not spend time waiting for the cache re-build. The value needs
 * to be an integer: just "1" or "0" without quotes.
 */
$bRenewCacheByDomainCall = 1;
