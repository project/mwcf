<?php

/**
 * Class mwcf (Multi Website Cache Flush)
 *
 * This class encapsulates some logic for the multiple website caching, which
 * are called by the functions that are set in mwcf_menu().
 */
class mwcf {

  /**
   * Initializes the config class members via the mwcf config file.
   */
  function __construct() {
    // Values will be filled from mwcf.conf.php.
    $aUrls                   = '';
    $bRenewCacheByDomainCall = '';
    $sCacheClearKey          = '';

    if (!is_file(conf_path() . '/mwcf.conf.php')) {

      $this->logAndDisplayConfigurationError();

      // Stop further execution.
      exit();
    }

    include_once(conf_path() . '/mwcf.conf.php');

    $this->aUrls                   = $aUrls;
    $this->bRenewCacheByDomainCall = $bRenewCacheByDomainCall;
    $this->sCacheClearKey          = $sCacheClearKey;
  }

  /**
   * This function clears all caches at all sites.
   *
   * @return null
   */
  public function clear_cache_everywhere() {
    $aUrlsForCacheClear = array();

    // Writes the messages for flushed cache into watchdog and echoes them.
    $action_messages = array();

    foreach ($this->aUrls as $url) {

      $url = $this->stripDashAtUrlEnd($url);

      $action_messages[] = 'Multi Website Cache Flush: Initialized cache clear ' .
          'on the following Drupal Website URL: ' . $url;

      $aUrlsForCacheClear[] = $url . '/ccall?cache_clear_key='
        . $this->sCacheClearKey;
    }

    $this->messageAndLogOutput($action_messages);

    new ParallelGet($aUrlsForCacheClear);

    if ($this->bRenewCacheByDomainCall === 1) {
      $this->renew_cache_by_domain_call();
    }

  }

  /**
   * This function initializes a simple cache rebuild by calling the specified
   * domains. To use this functionality, the $bRenewCacheByDomainCall-variable
   * needs be set to "1" in the config file.
   *
   * @return null
   */
  protected function renew_cache_by_domain_call() {
    $aDomainsToVisit = array();

    foreach ($this->aUrls as $url) {
      watchdog('info', 'Multi Website Cache Flush: Visited the following' .
        ' Domain for simple cache re-build: ' . $url);
      $aDomainsToVisit[] = $url;
    }

    new ParallelGet($aDomainsToVisit);
  }

  /**
   * This function clears the entire cache on the current site. It can be
   * called via appropriate cache clear key as get parameter or if the logged
   * in user has the appropriate permission.
   *
   * @return null
   */
  public function clear_all_caches() {
    if ((isset($_GET['cache_clear_key']) &&
      $this->sCacheClearKey == $_GET['cache_clear_key']) OR
      user_access('access url to flush entire cache on single site')
    ) {
      drupal_flush_all_caches();
      watchdog('info', 'Multi Website Cache Flush: Cleared all caches.');
    }
    else {
      watchdog('error', 'Multi Website Cache Flush: Wrong cache key' .
        ' or permission. Caches could not be flushed.');
    }

    if (user_access('access url to flush entire cache on single site')) {
      echo 'Multi Website Cache Flush: Entire single site cache flushed.';
    } else {
      echo 'Multi Website Cache Flush: Error - no sufficient permission.';
    }
  }

  /**
   * Informs the website administrator about configuration failure.
   *
   * @return null
   */
  protected function logAndDisplayConfigurationError() {
    $conf_error_message = 'Multi Website Cache Flush: The configuration file "mwcf.conf.php" cannot be found in the' .
        ' configuration directory (' . conf_path() . '). Copy the template configuration file (named' .
        ' "mwcf.template.conf.php") into the configuration directory and rename it to "mwcf.conf.php". Afterwards' .
        ' modify it to your needs (URLs, cache clear key).';

    watchdog('error', $conf_error_message);
    echo $conf_error_message . PHP_EOL;
  }

  /**
   * Strips the trailing slash at the end of an URL string.
   *
   * @param string $strUrl An URL to a Drupal website root address.
   * @return string
   */
  protected function stripDashAtUrlEnd($strUrl) {

    if (substr($strUrl, -1) == '/') {
      $strUrl = substr($strUrl, 0, -1);
    }

    return $strUrl;
  }

  /**
   * Outputs messages to administrative user and logs them.
   *
   * @param array $messages The messages to log and output.
   */
  protected function messageAndLogOutput($messages) {

    foreach ($messages as $message) {
      // Log
      watchdog('info', $message);
      // Output
      echo $message . '<br />';
    }

  }
}